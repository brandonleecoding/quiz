Readme

quiz Version 0.0.0

This is a simple program which asks a user a question, and evaluates the answer to produce different responses.
It's barebones now, but I have a few features I would like to explore as I learn (yes, these are my exercises. I know nothing.):
-Asking multiple questions
-Trying sequentially asking multiple questions with the computer serving as a 4th wall narrator
-Using the previous concept to make the quiz in to an interactive choose-your-own-adventure-trivia game. 
-Randomize which question is asked from a pool of questions and their answers. 
-Generate an error message when the user inputs something other than an integer.
-Think of other ideas to branch off from the logic currently in the program.

How to run:
1. Download the source with git pull or by following the linky.
2. Navigate to the directory containing quiz.c
3. Run:
	gcc quiz.c
	./a.out

I built it with GCC 7, although I am sure it will compile with other compilers. Why? Because it's such a simple fucking program and I can't imagine it not doing 
so?

