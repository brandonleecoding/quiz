/* This is a program to ask simple questions and verify the answer, */
/* Copyright Brandon Lee */
/* Licensed under the GNU GPL v3 */
/* Version 0.0.1. */

#include <stdio.h>

int main()
{
	int answer = 0;	/* initialize user answer variable as integer */


	printf( "What is the capital of Nevada?\n1. Reno\n2. Las Vegas\n3. Carson City\n4. Tonopah\n\n" );	/* Question */
	while ( answer != 3 ) {
		printf( "Answer with a number: ");				/* While the answer is not correct (the answer will not be correct at this point because we initialized the answer as zero, which will make this evaluation true */
		scanf( "%d", &answer );						/* Take the user's input in the form of an integer and assign it to the answer variable */
		if ( answer != 3 && answer < 5) {				/* If the numerical answer was 1, 2, or 4 (not three AND less than five). In Human: If the answer was a wrong answer within the available listed answers), */
			printf( "Try again!\n" );				/* Then the user will have to try again */
		}
		else if ( answer > 4 && answer != 777 ) {			/* if the answer was beyond the given answers, show an error. In order for the easter egg to work, we must use the boolean AND (&&). In order for the 'out of bounds' text to print, the answer must be any number greater than four AND exclude 777. Entering 777 would make this evaluation false, making the program move to the next condition. */
			printf( "That wasn't an option, pal...\n" );		/* be a smartass for no reason */
		}
		else if ( answer == 777 ) {					/* Easter Egg! */
			printf( "Well lookie here. You found the lucky message! Who thought there would be an easter egg in such a simple program?\n" );
		}
	}
	if ( answer == 3 ) {
		printf( "Correct! You win!\n" );	/* Success! */

	}
}
